.TH ACLGEN 1 "June 21, 1997"
.UC 4
.SH NAME
aclgen \- create optimized access lists

.SH SYNOPSIS
.B aclgen
.RB "[\|" \-h "\|]"
.RB "[\|" \-p "\|]"
.RB "[\|" \-i "\|]"
.RB "[\|" \-m\ \c
.I permit,deny\c
\&\|]
.RB "[\|" \-f\ \c
.I format-string\c
\&\|]
.RB "[\|" \-t\ \c
.I trace-flags\c
\&\|]
.RB "[\|"\c
.I input_file\c
\&\|]
.br

.SH DESCRIPTION
.B aclgen
builds optimized IP access lists. It is primarily intended for
use in large scripts that generate access lists, network lists,
or series of static routes from various input sources, e.g. RIPE
database
.I route
or
.I inetnum
objects.

.B aclgen
reads a series of IP address specifications, then computes the most
compact classless notation of listed address ranges.
The input address expressions may be inclusive or exclusive.
Meanwhile the program reads the input it builds internally a binary
tree representing the whole address space. After reading the input data,
.B aclgen
makes several optimizations on the tree on order to generate
the smallest possible graph corresponding to the input address
expresions.

Each input line may contain one address expression or a comment.
The program accepts wide variety of input formats.
.TP
address/prefix
.I address
is the usual dotted decimal representation,
.I prefix
is the number of significant bits between 0 and 32.
.TP
address mask
Similar to the above form but the signifcant bits are represented
by the dotted decimal
.I mask\c
\|. Contiguous netmasks only are allowed. However no matter if zero and one
bits are left or right. So 255.255.192.0 is equal to 0.0.63.255.
No difference between "mask" and "wildcard" specification.
So there is a minor ambiguity with
.I mask
0.0.0.0 (say 255.255.255.255).
In that case the program assumes 32 significant bits, if the address
is not 0.0.0.0, and thinks 0.0.0.0/0 otherwise.
(That means, that the address expression 0.0.0.0/32 has no eqivalent `\c
.I address mask\c
\|' form. However this is probably not a serious restriction.)

.TP
address-address
Inclusive range of addresses. The dash may be surrounded by any
number of spaces and/or tabulators.
.TP
address
The old classful address. However if the
.I address
does'n meet his "natural" netmask, i.e. "host part" is not zero,
the program treats the input expression as host address (\c
.I address\c
\|/32).
.LP
Address specifications may be preceded by a modifier.
Modifiers may be positive or negative. The default modifier is
positive. The acceptable input modifiers are

.nf
       positive   negative
       -------------------
       +          -
       permit     deny
       yes        no
.fi

Modifiers are case insensitive, the default is positive.

The input address list is preceded by an implicit
.br
.B "   deny 0.0.0.0/32"
.br
expression. In other words the generated filter list will discard
the unspecified part of the address space unless you override it
with an explicit
.br
.B "   permit 0.0.0.0/32"
.br
line in the input file. This behaviour is not affected by the
.B \-i
option. (See below.)

Empty lines, leading/trailing spaces and any characters
from `#' to the end of line are ignored as well as unparseable lines.

If the input line begins with `*'
.B aclgen
prints the currents state of the binary tree of the address space.
This is for debugging purposes only.

The input is read from
.I infile
or from the standard input if no input file specified.
`-' means stdin too.

.SH OPTIONS
.TP
.B \-h
Print version and usage then exit.
.TP
.B \-s
Silent mode. Warnings are supressed.
.TP
.B \-i
Invert modifiers of all input lines. It does'n affect the implicit
`deny 0.0.0.0/0' statement. (See above.)
.TP
.B \-p
Force "positive" output. If
.B \-p
is specified, the output contains no `deny' specifications.
Useful when generating routing tables or network lists.
.TP
.BI \-d " level"
Switch on diagnostics. `level' is the sum of one or more trace
flags:
.nf
    1  show input parsing
    2  print raw tree
    4  print optimized tree
   16  debug optimization step 1
   32  debug optimization step 2
   64  debug optimization step 3
  128  debug optimization step 4
.fi
.TP
.BI \-f " format-string"
.I Format-string
is a printf(3) like format specification of output lines.
The recognized conversion specifications are:

.nf
%a  address (dotted decimal)
%k  mask (dotted decimal)
%w  wildcard bits (dotted decimal, binary complement of %k)
%p  prefix
%m  modifier (permit/deny by default)
%%  the `%' itself
.fi

The default format string is "%m\ %a\ %w".
If you specify a format string without %m, the program
automatically turns on the
.B \-p
option.
.TP
.BI \-m " permit-string,deny-string"
Change the modifiers. The default modifiers are '\c
.I permit\c
\|' for addresses to accept and '\c
.I deny\c
\|  ' for addresses to reject.
.PP

.SH EXAMPLES
The examples below follow the syntax of Cisco IOS
configuration commands.

Basic functionality
.nf
  % \fIaclgen -f "access-list 83 %m %a %w" << END\fP
  > 192.168.10.0-192.168.15.0    # range of 6 C classes
  > 192.168.16.0/23              # classless
  > 192.168.18.0                 # classful
  > 192.168.19.0                 # classful
  > 192.168.32.0 255.255.224.0   # masked
  > 192.168.32.5                 # host
  > 192.168.80.7                 # host
  > END
  access-list 83 deny   192.168.8.0 0.0.1.255
  access-list 83 permit 192.168.8.0 0.0.7.255
  access-list 83 permit 192.168.16.0 0.0.3.255
  access-list 83 permit 192.168.32.0 0.0.31.255
  access-list 83 permit 192.168.80.7 0.0.0.0
  access-list 83 deny   0.0.0.0 255.255.255.255
  %
.fi

The same list but inverted
.nf
  % \fIaclgen -f "access-list 83 %m %a %w" -m "deny  ,permit" << END\fP
  > 192.168.10.0-192.168.15.0    # range of 6 C classes
  > 192.168.16.0/23              # classless
  > 192.168.18.0                 # classful
  > 192.168.19.0                 # classful
  > 192.168.32.0 255.255.224.0   # masked
  > 192.168.32.5                 # host
  > 192.168.80.7                 # host
  > END
  access-list 83 permit 192.168.8.0 0.0.1.255
  access-list 83 deny   192.168.8.0 0.0.7.255
  access-list 83 deny   192.168.16.0 0.0.3.255
  access-list 83 deny   192.168.32.0 0.0.31.255
  access-list 83 deny   192.168.80.7 0.0.0.0
  access-list 83 permit 0.0.0.0 255.255.255.255
  %
.fi

Classless BGP announcements
.nf
  % \fIaclgen -p -f "network %a %k" <<END\fP
  > 192.168.10.0-192.168.15.0    # range of 6 C classes
  > 192.168.16.0/23              # classless
  > 192.168.18.0                 # classful
  > 192.168.19.0                 # classful
  > 192.168.32.0 255.255.224.0   # masked
  > 192.168.32.5                 # host
  > 192.168.80.7                 # host
  END
  network 192.168.10.0 255.255.254.0
  network 192.168.12.0 255.255.252.0
  network 192.168.16.0 255.255.252.0
  network 192.168.32.0 255.255.224.0
  network 192.168.80.7 255.255.255.255
  %
.fi

Static routes
.nf
  % \fIaclgen -p -f "ip route %a %k 10.0.3.2" <<END\fP
  > 192.168.10.0-192.168.15.0    # range of 6 C classes
  > no 192.168.13.128/26         # hole in the block above
  > 192.168.16.0/23              # classless
  > 192.168.18.0                 # classful
  > 192.168.19.0                 # classful
  > 192.168.32.0 255.255.224.0   # masked
  > END
  ip route 192.168.10.0 255.255.254.0 10.0.3.2
  ip route 192.168.12.0 255.255.255.0 10.0.3.2
  ip route 192.168.13.0 255.255.255.128 10.0.3.2
  ip route 192.168.13.192 255.255.255.192 10.0.3.2
  ip route 192.168.14.0 255.255.254.0 10.0.3.2
  ip route 192.168.16.0 255.255.252.0 10.0.3.2
  ip route 192.168.32.0 255.255.224.0 10.0.3.2
  %
.fi

.SH BUGS
This manpage is written in "Hunglish". ;-)

.SH AUTHOR
Written by Gabor Kiss <kissg@sztaki.hu>

